﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Torre.Prueba.Helpers;
using Torre.Prueba.Models;

namespace Torre.Prueba.Controllers
{
    public class HomeController  : Controller
    {
        
        private readonly ISearchHelper _searchHelper;

        public HomeController(ISearchHelper searchHelper)
        {
           _searchHelper = searchHelper;
        }    

        public async Task<IActionResult> Index()
        {
            Sperson Persons= new Sperson();
            HttpClient client = _searchHelper.Initial();
            var content = new StringContent("", Encoding.UTF8, "application/json");
            HttpResponseMessage res = await client.PostAsync("people/_search/",content);

            if (res.IsSuccessStatusCode) 
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Persons = JsonConvert.DeserializeObject<Sperson>(results);
            }

            return View(Persons);
        }


        public IActionResult SearchP(String offset, String size,String[] agg)
        {
            return View();
        }



        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
