﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Member1
    {

        public string id { get; set; }
        public Person1 person { get; set; }
        public bool manager { get; set; }
        public bool poster { get; set; }
        public bool member { get; set; }
        public string status { get; set; }
        public bool visible { get; set; }
    }
}
