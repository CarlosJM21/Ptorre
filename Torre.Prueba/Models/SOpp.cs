﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class SOpp
    {
        public Aggregators aggregators { get; set; }
        public int offset { get; set; }
        public List<Result1> results { get; set; }
        public int size { get; set; }
        public int total { get; set; }
    }
}
