﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Group2
    {
        public string id { get; set; }
        public string text { get; set; }
        public string answer { get; set; }
        public int order { get; set; }
    }
}
