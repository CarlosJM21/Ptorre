﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class ProfessionalCultureGenomeResults
    {
        public List<Group2> groups { get; set; }
        public List<Analysis2> analyses { get; set; }
    }
}
