﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Organization
    {
        public int id { get; set; }
        public string name { get; set; }
        public string picture { get; set; }
    }
}
