﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Result1
    {
        public string id { get; set; }
        public string objective { get; set; }
        public string type { get; set; }
        public List<Organization> organizations { get; set; }
        public List<object> locations { get; set; }
        public bool remote { get; set; }
        public bool external { get; set; }
        public DateTime deadline { get; set; }
        public string status { get; set; }
        public Compens compensation { get; set; }
        public List<Skill> skills { get; set; }
        public List<Member> members { get; set; }
        public List<object> questions { get; set; }
        public Context context { get; set; }
    }
}
