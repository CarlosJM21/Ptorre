﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Metadata2
    {
        public double weight { get; set; }
        public int recommendations { get; set; }
        public string rel { get; set; }
    }
}
