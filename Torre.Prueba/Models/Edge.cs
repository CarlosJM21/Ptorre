﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Edge
    {
        public string source { get; set; }
        public string target { get; set; }
        public Metadata2 metadata { get; set; }
    }
}
