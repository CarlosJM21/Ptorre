﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Metadata
    {
        public string publicId { get; set; }
        public string subjectId { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public string picture { get; set; }
        public double weight { get; set; }
    }
}
