﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Graph
    {
        public bool directed { get; set; }
        public List<Node> nodes { get; set; }
        public List<Edge> edges { get; set; }
    }
}
