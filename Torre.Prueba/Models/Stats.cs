﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Stats
    {
        public int strengths { get; set; }
        public int education { get; set; }
        public int interests { get; set; }
        public int jobs { get; set; }
        public int projects { get; set; }
        public int achievements { get; set; }
    }
}
