﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Analysis2
    {
        public string groupId { get; set; }
        public string section { get; set; }
        public double analysis { get; set; }
    }
}
