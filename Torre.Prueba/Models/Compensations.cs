﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Compensations
    {
        public Employee employee { get; set; }
        public Freelancer freelancer { get; set; }
        public Intern intern { get; set; }
    }
}
