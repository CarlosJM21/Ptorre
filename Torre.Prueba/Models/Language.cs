﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class Language
    {
        public string code { get; set; }
        public string language { get; set; }
        public string fluency { get; set; }
     
    }
}
