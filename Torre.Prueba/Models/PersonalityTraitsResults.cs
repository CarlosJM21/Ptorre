﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Torre.Prueba.Models
{
    public class PersonalityTraitsResults
    {
        public List<Group> groups { get; set; }
        public List<Analysis> analyses { get; set; }
    }
}
