﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Torre.Prueba.Helpers
{
    public class SearchHelper: ISearchHelper
    {
        public HttpClient Initial()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://search.torre.co/");
            return client;
        }
               
    }
}
