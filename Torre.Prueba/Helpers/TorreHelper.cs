﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Torre.Prueba.Models;

namespace Torre.Prueba.Helpers
{
    public class TorreHelper : ITorreHelper
    {
        public async Task<Bio> bio(string Username)
        {
            Bio Biografia = new Bio();
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://torre.bio/api/bios/");
            
            HttpResponseMessage res = await client.GetAsync(Username);

            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Biografia = JsonConvert.DeserializeObject<Bio>(results);
            }
            return Biografia;
        }

        public async Task<Red> Contactos(string username, int? proof)
        {
            Red r = new Red();
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://torre.bio/api/people/");
            String Url= username+"/network";
            if (proof != 0) 
            {
                Url += "?" + proof;
            }
            HttpResponseMessage res = await client.GetAsync(Url);

            return r;
        }

        public Task<Opp> Job(string id)
        {
            throw new NotImplementedException();
        }

        public Task<SOpp> searchoppt(string offset, string size, string[] agg)
        {
            throw new NotImplementedException();
        }

        public Task<Sperson> searchpeople(string offset, string size, string[] agg)
        {
            throw new NotImplementedException();
        }

        Task<Person> ITorreHelper.bio(string Username)
        {
            throw new NotImplementedException();
        }

        Task<Person> ITorreHelper.Contactos(string username, int? proof)
        {
            throw new NotImplementedException();
        }

        Task<Job> ITorreHelper.Job(string id)
        {
            throw new NotImplementedException();
        }

        Task<Job> ITorreHelper.searchoppt(string offset, string size, string[] agg)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<Person>> ITorreHelper.searchpeople(string offset, string size, string[] agg)
        {
            throw new NotImplementedException();
        }
    }
}
