﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Torre.Prueba.Models;

namespace Torre.Prueba.Helpers
{
    public interface ITorreHelper
    {

        Task<IEnumerable<Person>> searchpeople (String offset, String size, String[] agg);

        Task<Job> searchoppt(String offset, String size, String[] agg);

        Task<Person> bio(String Username);

        Task<Job> Job(String id);

        Task<Person> Contactos(String username,int? proof);

    }
}
